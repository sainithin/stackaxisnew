const mongoose = require("mongoose");

const ClientSchema = new mongoose.Schema({
    username: { type: String},
    gateway:String,
    port:String,
    expirydate:{type:Date},
    ipad:String,
    package:String,
    pusername:String,
    ppassword:String,
    skype: String,
    cname: String,
    bhw: String,
    mps:String,
    createdBy:String,
    comments: String
});

module.exports = mongoose.model("Super", ClientSchema);
