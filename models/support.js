const mongoose = require("mongoose"),
    passportLocalMongoose = require("passport-local-mongoose");

const SupportSchema = new mongoose.Schema({
    username: { type: String},
    email: { type: String},
    query:String,
    phone:Number,
    status:{type:String,default:"Pending"},
    isAdmin:{type:String,default:false},
});





SupportSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("Support", SupportSchema);
